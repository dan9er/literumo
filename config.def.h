#pragma once

#include <stdlib.h>

#include <espeak-ng/speak_lib.h>

/* word buffer length (w/o \0 terminator) */
#define WORDLEN 31

/* prompt buffer length (w/o \0 terminator) */
#define PROMPTLEN 127

/* audio buffer length (ms) */
#define AUDLEN 1000

/* espeak_data folder (NULL for default install location) */
#define DATA_FOLDER NULL

/* change in pitch for CAPITAL words (3-50, 0 to disable) */
#define CAPITAL_PITCH 50

/* voice (see eSpeak documentation) */
static const espeak_VOICE VOICE = {NULL, "en-us", NULL, 0, 0, 0};
