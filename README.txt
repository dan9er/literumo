literumo (Esperanto for "spelling")
===================================

Talking spelling tester, using eSpeak NG

Features
--------
-	130 cloc(1) lines of C99, including config
-	Reads words from a specially formatted TSV file
-	Speaks prompts using eSpeak NG (Can't get clues as if using written prompts)
-	Can speak a prompt again if misheard
-	Avoids using dynamic memory
-	Configured with a config.h

Anti-Features
-------------
-	Little tolerence for misformating of the word file (see Usage)
-	Does not keep track of WHICH words were spelt incorrectly

Requirements
------------
In order to build literumo, you'll need eSpeak NG's header files.

To run it, you also need eSpeak's voice data files, the path to of which can be
configured in config.h. (If you have eSpeak NG installed on your system, you can
just leave it as NULL.)

Building & Installation
-----------------------
Copy config.def.mk to config.mk, then edit that to match your local setup using
your text editor of choice. Then:

	make
	make install

(Of course, install with root permissions if needed)

Usage
-----
Make a TSV file of words and prompts, in this format ('>' represents a tab
character):

	foo>the first placeholder variable name
	bar>he washed himself with a, BLANK, of soap
	farb>farb

The first "column" is the word to test for, and the second is the prompt to be
spoken (this can be anything, but above you can see 3 different strategies).

Both of these MUST fit in literumo's static buffers, the lengths of which are
defined (and can be raised if desired) in config.h.

By default, words with a Capital Letter IN aNy poSsITon will be spoken
in a higher pitch; this can be configured or disabled in config.h.

The file MUST have a tailing endline.

Pass this TSV file to literumo:

	literumo {words.tsv}

literumo will go through the words in order. After the prompt has finished
speaking, type the word (hopefully spelt correctly!) and hit enter.

To repeat the prompt, enter exactly "?" as your answer.

At the end, literumo will print your score and exit.

Repository
----------

	git clone https://gitlab.com/dan9er/literumo.git
