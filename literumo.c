/* See LICENSE file for copyright and license details. */
#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include <espeak-ng/speak_lib.h>

#include "config.h"

static char *argv0;

static inline void verr(const char *fmt, va_list ap);
static void die(const char *fmt, ...);
static inline void usage(void);

static inline unsigned read_word(char *restrict answer, char *restrict prompt,
	FILE *restrict stream);

static inline void init(void);
static void speak(const char *text);

static inline void
verr(const char *fmt, va_list ap)
{
	if (argv0)
		fprintf(stderr, "%s: ", argv0);

	vfprintf(stderr, fmt, ap);

	if (fmt[0] && fmt[strlen(fmt) - 1] == ':') {
		fputc(' ', stderr);
		perror(NULL);
	} else {
		fputc('\n', stderr);
	}
}

static void
die(const char *fmt, ...)
{
	va_list ap;

	va_start(ap, fmt);
	verr(fmt, ap);
	va_end(ap);

	exit(1);
}

static inline void
usage(void)
{
	fprintf(stderr, "usage: %s {words.tsv}", argv0);
	exit(1);
}

static inline unsigned
read_word(char *restrict answer, char *restrict prompt, FILE *restrict stream)
{
	int c;

	if ((c = fgetc(stream)) == EOF)
		return 0;
	else
		if (ungetc(c, stream) == EOF)
			die("ungetc: Error");

	while ((c = fgetc(stream)) != EOF && c != '\t')
		*(answer++) = c;
	if (ferror(stream))
		die("fgetc: Error");
	else if (feof(stream))
		die("fgetc: Unexpected end of file");
	*answer = '\0';

	while ((c = fgetc(stream)) != EOF && c != '\n')
		*(prompt++) = c;
	if (ferror(stream))
		die("fgetc: Error");
	else if (feof(stream))
		die("fgetc: Unexpected end of file");
	*prompt = '\0';

	return 1;
}

static inline void
init(void)
{
	if (espeak_Initialize(AUDIO_OUTPUT_SYNCH_PLAYBACK,
		AUDLEN, DATA_FOLDER, 0x0) == EE_INTERNAL_ERROR)
		die("init: Error initing espeak");

	if (espeak_SetVoiceByProperties(&VOICE) == EE_INTERNAL_ERROR)
		die("init: Error setting voice");

	/* high pitch on CAPITALS
	 * TODO always returns EE_INTERNAL_ERROR for some dumb reason */
	espeak_SetParameter(espeakCAPITALS, CAPITAL_PITCH, 0);
}

static void
speak(const char *text)
{
	if (espeak_Synth(text, 0,0,0x0,0, espeakCHARS_8BIT|espeakENDPAUSE,
		NULL,NULL))
		die("speak: Error synthesizing speech");
}

int
main(int argc, char *argv[])
{
	FILE *words;
	char  answer  [WORDLEN+1],
	      response[WORDLEN+1],
	      prompt  [PROMPTLEN+1];
	unsigned score = 0,
	         total = 0;

	argv0 = argv[0];
	if (argc != 2)
		usage();

	words = fopen(argv[1], "r");
	if (words == NULL)
		die("fopen:");

	init();

	while (read_word(answer, prompt, words)) {
		++total;

		do {
			speak(prompt);
			fgets(response, WORDLEN+1, stdin);
			if (ferror(stdin))
				die("fgets:");

			for (char *i = response; 1; ++i) {
				if (*i == '\n') {
					*i = '\0';
					break;
				}
			}
		} while (!strcmp(response, "?"));

		if (!strcmp(response, answer))
			++score;
	}

	printf("%u/%u", score, total);
	speak("finish");
	return 0;
}
