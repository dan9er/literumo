# literumo version
VERSION = 0

# Copy config.def.mk to config.mk, then customize below to fit your system

# compiler and linker
CC = cc

# install path
PREFIX = /usr/local

# flags
CPPFLAGS        = -D_DEFAULT_SOURCE
CFLAGS          = -std=c99 -pedantic -Wall -Wextra -Wpedantic
LDFLAGS         = -s
ESPEAKNG-LDLIBS = -lespeak-ng

# release flags
CFLAGS += -Os

# debug flags
#CFLAGS  += -O0 -g3
#LDFLAGS += -g3
