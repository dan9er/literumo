# See LICENSE.txt for copyright & license details.

.POSIX:

include config.mk

all: literumo

literumo-LDLIBS = $(ESPEAKNG-LDLIBS)

literumo: literumo.o
literumo.o: literumo.c config.mk config.h

config.h:
	cp config.def.h $@

.o:
	$(CC) -o $@ $(LDFLAGS) $< $($*-LDLIBS)

.c.o:
	$(CC) -c $(CPPFLAGS) $(CFLAGS) $<

clean:
	rm -f literumo literumo.o

dist:
	rm -rf "literumo-$(VERSION)"
	mkdir -p "literumo-$(VERSION)"
	cp -R LICENSE.txt Makefile README.txt config.def.mk \
	      config.def.h literumo.c "literumo-$(VERSION)"
	tar -cf - "literumo-$(VERSION)" | gzip -c > "literumo-$(VERSION).tar.gz"
	rm -rf "literumo-$(VERSION)"

install: all
	mkdir -p "$(DESTDIR)$(PREFIX)/bin"
	cp -f literumo "$(DESTDIR)$(PREFIX)/bin"
	chmod 755 "$(DESTDIR)$(PREFIX)/bin/literumo"

uninstall:
	rm -f "$(DESTDIR)$(PREFIX)/bin/literumo"

.PHONY: all clean dist install uninstall
